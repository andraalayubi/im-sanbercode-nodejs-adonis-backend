//Tugas nomor 1
var nama = "and";
var peran = "";

if(nama == "" && peran == ""){
    console.log("Nama Harus diisi!");
}else if(nama != ""){
    if(peran == ""){
        console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
    }else if (peran == "Penyihir"){
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf`);
    } else if (peran == "Guard"){
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf`);
    } else if (peran == "Werewolf"){
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log(`Halo Werewolf ${nama}, kamu akan memakan mangsa setiap malam`);
    }
}

//Tugas nomor 2
var hari = 21; 
var bulan = 1; 
var tahun = 1945;

if (1900>=tahun&&tahun<=2200){
    switch (bulan){
        case 1:
            if (1>=hari&&hari<=31){
                console.log(`${hari} Januari ${tahun}`);
            }
            break;
        case 2:
            if (1>=hari&&hari<=28){
                console.log(`${hari} Februari ${tahun}`);
            }
            break;
        case 3:
            if (1>=hari&&hari<=31){
                console.log(`${hari} Maret ${tahun}`);
            }
            break;
        case 4:
            if (1>=hari&&hari<=31){
                console.log(`${hari} April ${tahun}`);
            }
            break;
        case 5:
            if (1>=hari&&hari<=30){
                console.log(`${hari} Mei ${tahun}`);
            }
            break;
        case 6:
            if (1>=hari&&hari<=31){
                console.log(`${hari} Juni ${tahun}`);
            }
            break;
        case 7:
            if (1>=hari&&hari<=30){
                console.log(`${hari} Juli ${tahun}`);
            }
            break;
        case 8:
            if (1>=hari&&hari<=31){
                console.log(`${hari} Agustus ${tahun}`);
            }
            break;
        case 9:
            if (1>=hari&&hari<=30){
                console.log(`${hari} September ${tahun}`);
            }
            break;
        case 10:
            if (1>=hari&&hari<=31){
                console.log(`${hari} Oktober ${tahun}`);
            }
            break;
        case 11:
            if (1>=hari&&hari<=30){
                console.log(`${hari} November ${tahun}`);
            }
            break;
        case 12:
            if (1>=hari&&hari<=31){
                console.log(`${hari} Desember ${tahun}`);
            }
            break;       
        default:
            console.log("Harap masukkan dan periksa tanggal, bulan, dan tahun")
            break;
}
}